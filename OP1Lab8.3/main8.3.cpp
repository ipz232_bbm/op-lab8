#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <Windows.h>
#include <math.h>

int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	double res;

	for (int x = 5; x <= 7; x++) {
		res = 1 / pow(x, 1./3);
		printf("1/pow(x, 1./3) �� ��������� ����� for: %f\n", res);
	}

	printf("\n");

	{
	int x = 5;
	while (x <= 7) {
		res = 1 / pow(x, 1. / 3);
		x++;
		printf("1/pow(x, 1./3) �� ��������� ����� while: %f\n", res);
	}}
	
	printf("\n");

	for (double x = 2; x <= 3.05; x += 0.1) {
		res = exp(x*x)/2;
		printf("exp(x * x) / 2 �� ��������� ����� for: %f\n", res);
	}

	printf("\n");

	{
	double x = 2;
	while (x <= 3.05) {
		res = exp(x * x) / 2;
		x += 0.1;
		printf("exp(x * x) / 2 �� ��������� ����� while: %f\n", res);
	}}

	return 0;
}