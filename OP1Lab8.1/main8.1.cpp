#include <stdio.h>
#include <Windows.h>

int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	unsigned long long mult = 1;

	for (int i = 2; i <= 30; i += 2) {
		mult *= i;
	}
	printf("��������� �� ��������� ����� for: %lld\n", mult);

	mult = 1;

	int i = 2;
	while (i <= 30) {
		mult *= i;
		i += 2;
	}
	printf("%��������� �� ��������� ����� while: %lld\n", mult);
	return 0;
}