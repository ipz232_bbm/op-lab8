#include <stdio.h>
#include <Windows.h>

int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	int n, i;
	scanf_s("%d", &n);
	unsigned power = 1;
	i = n;

	while (n-- >= 1) power *= 2;
	printf("��������� �� ��������� ����� while: %u\n", power);

	for (power = 1; i >= 1; i--) power *= 2;
	printf("��������� �� ��������� ����� for: %u\n", power);

	return 0;
}